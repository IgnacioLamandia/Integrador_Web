package tos

import model.Tendencia
import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class TendenciaReducida {
	
	String numero
	String fecha
	
	new(Tendencia tendencia) {
		this.fecha = tendencia.getFecha().getHours().toString() + ":" +
					 tendencia.getFecha().getMinutes().toString()
		this.numero = tendencia.getNumero().toString()

	}
	
	new()
	{
		this.fecha = ""
		this.numero = ""
	}

}