package controllers

import appModel.El8DeLaRiveraAppModel
import org.uqbar.xtrest.api.Result
import org.uqbar.xtrest.api.annotation.Body
import org.uqbar.xtrest.api.annotation.Controller
import org.uqbar.xtrest.api.annotation.Get
import org.uqbar.xtrest.api.annotation.Post
import org.uqbar.xtrest.json.JSONUtils
import tos.UsuarioReducido
import tos.ApuestaReducida
import tos.TendenciaReducida

@Controller
class El8DeLaRiveraController {
	
	extension JSONUtils = new JSONUtils
	private El8DeLaRiveraAppModel appModel
	
	new(El8DeLaRiveraAppModel appModel){
		this.appModel = appModel
	}
			
	@Post("/usuarios")
    def logearUsuario(@Body String body){
        response.contentType = "application/json"
        try
        {
          	var UsuarioReducido usuario = body.fromJson(typeof(UsuarioReducido))
        	val nombre = usuario.getNombre()
        	var password = usuario.getPassword()
			appModel.logearUsuario(nombre, password)
			ok(usuario.toJson)
     	}
     	catch(IllegalAccessException ex)
     	{
         	forbidden('{ "error": "Usuario o contraseña incorrecto" }')
		}
    }
     
    @Post("/apostar")
	def realizarApuesta(String nombre, String monto, String numero)
    {
        response.contentType = "application/json"
    	if(monto != null && numero != null)
		{
			appModel.apostar(nombre, monto, numero);
    		ok('"Apuesta realizada"')
		}
		else
		{
			badRequest('{ "error": "No se informa el monto o el numero" }')            	
    	}
    }
     
    @Get("/usuario")
	def Result usuario(String nombre) 
	{
		val res = this.appModel.repositorio.getUsuario(nombre)
		ok(res.toJson)
	}
	
	 @Get("/usuario/saldo")
	def Result getSaldoU(String nombre) 
	{
		val res = this.appModel.repositorio.getUsuario(nombre).getSaldo()
		ok(res.toJson)
	}

	@Get("/usuario/apuestas")
	def Result apuestasDelUsuario(String nombre) 
	{
		val apuestas = this.appModel.getApuetasUsuario(nombre)
		
		val res = apuestas.map[new ApuestaReducida(it)]
		ok(res.toJson)
	}
	
	@Get("/usuario/apuestas/resultado")
	def Result getResultadoUltimaApuesta(String nombre) 
	{
		val apuestas = this.appModel.getApuetasUsuario(nombre)
		val apuesta = apuestas.get(apuestas.size()-1)
		val res = new ApuestaReducida(apuesta)
		ok(res.toJson)
	}
	
	@Get("/apuestas")
	def Result apuestasTotales() 
	{
		val apuestas = this.appModel.getTotalApuetas()
		
		val res = apuestas.map[new ApuestaReducida(it)]
		ok(res.toJson)
	}
	
	@Get("/apuestas/tendencias")
	def Result tendenciasTotales() 
	{
		val tendencias = this.appModel.getTotalTendencias()
		
		val res = tendencias.map[new TendenciaReducida(it)]
		ok(res.toJson)
	}    

}