var app =angular.module("apuestasApp", []);

  app.controller("apuestasController", function($scope, apuestasService) {
    var self = this;
    this.usuario = {};
    this.apuestasU = [];
    
    this.apuestas = [];
    this.tendencias = [];
    
    $scope.monto = 0;
    $scope.numero = 0;
    this.apuesta = {};

    $scope.getUsuario = function(){
        apuestasService.getUsuario("Fulano", function(data){
        	self.usuario = data;
      });
    }

    $scope.getSaldoU = function(){
        apuestasService.getSaldoU(self.usuario.nombre, function(data){
        self.usuario.saldo = data;
      });
    }

    $scope.getApuestasU = function(){
        apuestasService.getApuestasU(self.usuario.nombre, function(data){
        self.apuestasU = data;
      });
    }
    
    $scope.getResultadoUltimaApuesta = function(){
        apuestasService.getResultadoUltimaApuesta(self.usuario.nombre, function(data){
        self.apuesta = data;

      });
    }

    $scope.getApuestas = function(){
        apuestasService.getApuestas(function(data){
        self.apuestas = data;
      });
    }

    $scope.getTendencias = function(){
        apuestasService.getTendencias(function(data){
        self.tendencias = data;
      });
    }

    $scope.apostar = function(){
        apuestasService.apostar(self.usuario.nombre, $scope.monto, $scope.numero, function(data){
        $scope.getSaldoU();
       	$scope.getResultadoUltimaApuesta();
        $scope.getTendencias();

      });
    }
    
    $scope.getUsuario();
    //$scope.getApuestasU();
    //$scope.getApuestas();
    $scope.getTendencias();

  });

    app.controller("loginController", function($scope, $window, $location, loginService) {
  	
      self = this;
      $scope.nombre = "";
      $scope.password = "";
      
      $scope.logearUsuario = function(){
        loginService.logearUsuario($scope.nombre, $scope.password, function(data){
          $window.location.href = "apostar.html";
      });
    }
  });