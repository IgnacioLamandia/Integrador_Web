package tos;

import java.util.Date;
import model.Apuesta;
import model.Resultado;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;

@Accessors
@SuppressWarnings("all")
public class ApuestaReducida {
  private String monto;
  
  private String numero;
  
  private String fecha;
  
  private String resultado;
  
  public ApuestaReducida(final Apuesta apuesta) {
    int _monto = apuesta.getMonto();
    String _string = Integer.valueOf(_monto).toString();
    this.monto = _string;
    int _numero = apuesta.getNumero();
    String _string_1 = Integer.valueOf(_numero).toString();
    this.numero = _string_1;
    Date _fecha = apuesta.getFecha();
    String _string_2 = _fecha.toString();
    this.fecha = _string_2;
    Resultado _resultado = apuesta.getResultado();
    String _mensaje = _resultado.getMensaje();
    this.resultado = _mensaje;
  }
  
  public ApuestaReducida() {
    this.monto = "";
    this.numero = "";
    this.fecha = "";
    this.fecha = "";
  }
  
  @Pure
  public String getMonto() {
    return this.monto;
  }
  
  public void setMonto(final String monto) {
    this.monto = monto;
  }
  
  @Pure
  public String getNumero() {
    return this.numero;
  }
  
  public void setNumero(final String numero) {
    this.numero = numero;
  }
  
  @Pure
  public String getFecha() {
    return this.fecha;
  }
  
  public void setFecha(final String fecha) {
    this.fecha = fecha;
  }
  
  @Pure
  public String getResultado() {
    return this.resultado;
  }
  
  public void setResultado(final String resultado) {
    this.resultado = resultado;
  }
}
