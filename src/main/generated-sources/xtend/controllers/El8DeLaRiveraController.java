package controllers;

import appModel.El8DeLaRiveraAppModel;
import com.google.common.base.Objects;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Apuesta;
import model.Repositorio;
import model.Tendencia;
import model.Usuario;
import org.eclipse.jetty.server.Request;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.uqbar.xtrest.api.Result;
import org.uqbar.xtrest.api.annotation.Body;
import org.uqbar.xtrest.api.annotation.Controller;
import org.uqbar.xtrest.api.annotation.Get;
import org.uqbar.xtrest.api.annotation.Post;
import org.uqbar.xtrest.json.JSONUtils;
import org.uqbar.xtrest.result.ResultFactory;
import tos.ApuestaReducida;
import tos.TendenciaReducida;
import tos.UsuarioReducido;

@Controller
@SuppressWarnings("all")
public class El8DeLaRiveraController extends ResultFactory {
  @Extension
  private JSONUtils _jSONUtils = new JSONUtils();
  
  private El8DeLaRiveraAppModel appModel;
  
  public El8DeLaRiveraController(final El8DeLaRiveraAppModel appModel) {
    this.appModel = appModel;
  }
  
  @Post("/usuarios")
  public Result logearUsuario(@Body final String body, final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      response.setContentType("application/json");
      Result _xtrycatchfinallyexpression = null;
      try {
        Result _xblockexpression_1 = null;
        {
          UsuarioReducido usuario = this._jSONUtils.<UsuarioReducido>fromJson(body, UsuarioReducido.class);
          final String nombre = usuario.getNombre();
          String password = usuario.getPassword();
          this.appModel.logearUsuario(nombre, password);
          String _json = this._jSONUtils.toJson(usuario);
          _xblockexpression_1 = ResultFactory.ok(_json);
        }
        _xtrycatchfinallyexpression = _xblockexpression_1;
      } catch (final Throwable _t) {
        if (_t instanceof IllegalAccessException) {
          final IllegalAccessException ex = (IllegalAccessException)_t;
          _xtrycatchfinallyexpression = ResultFactory.forbidden("{ \"error\": \"Usuario o contraseña incorrecto\" }");
        } else {
          throw Exceptions.sneakyThrow(_t);
        }
      }
      _xblockexpression = _xtrycatchfinallyexpression;
    }
    return _xblockexpression;
  }
  
  @Post("/apostar")
  public Result realizarApuesta(final String nombre, final String monto, final String numero, final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      response.setContentType("application/json");
      Result _xifexpression = null;
      if (((!Objects.equal(monto, null)) && (!Objects.equal(numero, null)))) {
        Result _xblockexpression_1 = null;
        {
          this.appModel.apostar(nombre, monto, numero);
          _xblockexpression_1 = ResultFactory.ok("\"Apuesta realizada\"");
        }
        _xifexpression = _xblockexpression_1;
      } else {
        _xifexpression = ResultFactory.badRequest("{ \"error\": \"No se informa el monto o el numero\" }");
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  @Get("/usuario")
  public Result usuario(final String nombre, final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      Repositorio _repositorio = this.appModel.getRepositorio();
      final Usuario res = _repositorio.getUsuario(nombre);
      String _json = this._jSONUtils.toJson(res);
      _xblockexpression = ResultFactory.ok(_json);
    }
    return _xblockexpression;
  }
  
  @Get("/usuario/saldo")
  public Result getSaldoU(final String nombre, final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      Repositorio _repositorio = this.appModel.getRepositorio();
      Usuario _usuario = _repositorio.getUsuario(nombre);
      final int res = _usuario.getSaldo();
      String _json = this._jSONUtils.toJson(Integer.valueOf(res));
      _xblockexpression = ResultFactory.ok(_json);
    }
    return _xblockexpression;
  }
  
  @Get("/usuario/apuestas")
  public Result apuestasDelUsuario(final String nombre, final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      final List<Apuesta> apuestas = this.appModel.getApuetasUsuario(nombre);
      final Function1<Apuesta, ApuestaReducida> _function = new Function1<Apuesta, ApuestaReducida>() {
        public ApuestaReducida apply(final Apuesta it) {
          return new ApuestaReducida(it);
        }
      };
      final List<ApuestaReducida> res = ListExtensions.<Apuesta, ApuestaReducida>map(apuestas, _function);
      String _json = this._jSONUtils.toJson(res);
      _xblockexpression = ResultFactory.ok(_json);
    }
    return _xblockexpression;
  }
  
  @Get("/usuario/apuestas/resultado")
  public Result getResultadoUltimaApuesta(final String nombre, final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      final List<Apuesta> apuestas = this.appModel.getApuetasUsuario(nombre);
      int _size = apuestas.size();
      int _minus = (_size - 1);
      final Apuesta apuesta = apuestas.get(_minus);
      final ApuestaReducida res = new ApuestaReducida(apuesta);
      String _json = this._jSONUtils.toJson(res);
      _xblockexpression = ResultFactory.ok(_json);
    }
    return _xblockexpression;
  }
  
  @Get("/apuestas")
  public Result apuestasTotales(final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      final List<Apuesta> apuestas = this.appModel.getTotalApuetas();
      final Function1<Apuesta, ApuestaReducida> _function = new Function1<Apuesta, ApuestaReducida>() {
        public ApuestaReducida apply(final Apuesta it) {
          return new ApuestaReducida(it);
        }
      };
      final List<ApuestaReducida> res = ListExtensions.<Apuesta, ApuestaReducida>map(apuestas, _function);
      String _json = this._jSONUtils.toJson(res);
      _xblockexpression = ResultFactory.ok(_json);
    }
    return _xblockexpression;
  }
  
  @Get("/apuestas/tendencias")
  public Result tendenciasTotales(final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) {
    Result _xblockexpression = null;
    {
      final List<Tendencia> tendencias = this.appModel.getTotalTendencias();
      final Function1<Tendencia, TendenciaReducida> _function = new Function1<Tendencia, TendenciaReducida>() {
        public TendenciaReducida apply(final Tendencia it) {
          return new TendenciaReducida(it);
        }
      };
      final List<TendenciaReducida> res = ListExtensions.<Tendencia, TendenciaReducida>map(tendencias, _function);
      String _json = this._jSONUtils.toJson(res);
      _xblockexpression = ResultFactory.ok(_json);
    }
    return _xblockexpression;
  }
  
  public void handle(final String target, final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
    {
    	Matcher matcher = 
    		Pattern.compile("/usuario").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Get") && matcher.matches()) {
    		// take parameters from request
    		String nombre = request.getParameter("nombre");
    		
    		// take variables from url
    		
    		
    	    Result result = usuario(nombre, target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/usuario/saldo").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Get") && matcher.matches()) {
    		// take parameters from request
    		String nombre = request.getParameter("nombre");
    		
    		// take variables from url
    		
    		
    	    Result result = getSaldoU(nombre, target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/usuario/apuestas").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Get") && matcher.matches()) {
    		// take parameters from request
    		String nombre = request.getParameter("nombre");
    		
    		// take variables from url
    		
    		
    	    Result result = apuestasDelUsuario(nombre, target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/usuario/apuestas/resultado").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Get") && matcher.matches()) {
    		// take parameters from request
    		String nombre = request.getParameter("nombre");
    		
    		// take variables from url
    		
    		
    	    Result result = getResultadoUltimaApuesta(nombre, target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/apuestas").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Get") && matcher.matches()) {
    		// take parameters from request
    		
    		// take variables from url
    		
    		
    	    Result result = apuestasTotales(target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/apuestas/tendencias").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Get") && matcher.matches()) {
    		// take parameters from request
    		
    		// take variables from url
    		
    		
    	    Result result = tendenciasTotales(target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/usuarios").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Post") && matcher.matches()) {
    		// take parameters from request
    		String body = readBodyAsString(request);
    		
    		// take variables from url
    		
    		
    	    Result result = logearUsuario(body, target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    {
    	Matcher matcher = 
    		Pattern.compile("/apostar").matcher(target);
    	if (request.getMethod().equalsIgnoreCase("Post") && matcher.matches()) {
    		// take parameters from request
    		String nombre = request.getParameter("nombre");
    		String monto = request.getParameter("monto");
    		String numero = request.getParameter("numero");
    		
    		// take variables from url
    		
    		
    	    Result result = realizarApuesta(nombre, monto, numero, target, baseRequest, request, response);
    	    result.process(response);
    	    
    		response.addHeader("Access-Control-Allow-Origin", "*");
    	    baseRequest.setHandled(true);
    	    return;
    	}
    }
    this.pageNotFound(baseRequest, request, response);
  }
  
  public void pageNotFound(final Request baseRequest, final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
    response.getWriter().write(
    	"<html><head><title>XtRest - Page Not Found!</title></head>" 
    	+ "<body>"
    	+ "	<h1>Page Not Found !</h1>"
    	+ "	Supported resources:"
    	+ "	<table>"
    	+ "		<thead><tr><th>Verb</th><th>URL</th><th>Parameters</th></tr></thead>"
    	+ "		<tbody>"
    	+ "			<tr>"
    	+ "				<td>GET</td>"
    	+ "				<td>/usuario</td>"
    	+ "				<td>nombre</td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>GET</td>"
    	+ "				<td>/usuario/saldo</td>"
    	+ "				<td>nombre</td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>GET</td>"
    	+ "				<td>/usuario/apuestas</td>"
    	+ "				<td>nombre</td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>GET</td>"
    	+ "				<td>/usuario/apuestas/resultado</td>"
    	+ "				<td>nombre</td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>GET</td>"
    	+ "				<td>/apuestas</td>"
    	+ "				<td></td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>GET</td>"
    	+ "				<td>/apuestas/tendencias</td>"
    	+ "				<td></td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>POST</td>"
    	+ "				<td>/usuarios</td>"
    	+ "				<td>body</td>"
    	+ "			</tr>"
    	+ "			<tr>"
    	+ "				<td>POST</td>"
    	+ "				<td>/apostar</td>"
    	+ "				<td>nombre, monto, numero</td>"
    	+ "			</tr>"
    	+ "		</tbody>"
    	+ "	</table>"
    	+ "</body>"
    );
    response.setStatus(404);
    baseRequest.setHandled(true);
  }
}
