package tos

import org.eclipse.xtend.lib.annotations.Accessors

@Accessors
class UsuarioReducido{
	
	String nombre
	String password
	
	new(String nombre, String password) {
		this.nombre = nombre
		this.password = password
	}
	
	new()
	{
		this.nombre = ""
		this.password = ""
	}

}
