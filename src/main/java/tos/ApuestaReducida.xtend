package tos

import org.eclipse.xtend.lib.annotations.Accessors
import model.Apuesta

@Accessors
class ApuestaReducida {
	
	String monto
	String numero
	String fecha
	String resultado
	
	new(Apuesta apuesta) {
		this.monto = apuesta.getMonto().toString()
		this.numero = apuesta.getNumero().toString()
		this.fecha = apuesta.getFecha().toString()
		this.resultado = apuesta.getResultado().getMensaje()
	}
	
	new()
	{
		this.monto = ""
		this.numero = ""
		this.fecha = ""
		this.fecha = ""
		
	}

}