package controllers

import java.util.ArrayList
import model.Repositorio
import model.Usuario
import appModel.El8DeLaRiveraAppModel
import org.uqbar.xtrest.api.XTRest
import model.Apuesta
import model.Resultado
import model.Tendencia

class El8DeLaRiveraApp {
	
	def static void main(String[] args) {	
			
		val fulano  = new Usuario("Fulano",  "123456")
		val sultano = new Usuario("Sultano", "123456")
		val mengano = new Usuario("Mengano", "123456")
		
		val apuesta1 = new Apuesta(100, 5)
		val apuesta2 = new Apuesta(200, 8)
		val apuesta3 = new Apuesta(300, 12)
		val apuesta4 = new Apuesta(400, 26)
		val apuesta5 = new Apuesta(500, 30)
		val apuesta6 = new Apuesta(600, 33)
		
		val resultado1 = new Resultado(1)
		val resultado2 = new Resultado(2)
		val resultado3 = new Resultado(3)
		val resultado4 = new Resultado(4)
		val resultado5 = new Resultado(5)
		val resultado6 = new Resultado(6)
		
		apuesta1.setResultado(resultado1)
		apuesta2.setResultado(resultado2)
		apuesta3.setResultado(resultado3)
		apuesta4.setResultado(resultado4)
		apuesta5.setResultado(resultado5)
		apuesta6.setResultado(resultado6)
		
		fulano.saldo  = 2000
		sultano.saldo = 3000
		mengano.saldo = 4000
		
		var apuestas1 = new ArrayList<Apuesta>();
		apuestas1.add(apuesta1)
		apuestas1.add(apuesta2)
		apuestas1.add(apuesta3)
		var apuestas2 = new ArrayList<Apuesta>();
		apuestas2.add(apuesta4)
		apuestas2.add(apuesta5)
		var apuestas3 = new ArrayList<Apuesta>();
		apuestas3.add(apuesta6)

		fulano.apuestas  = apuestas1
		sultano.apuestas = apuestas2
		mengano.apuestas = apuestas3
		
		var usuarios = new ArrayList<Usuario>();	
		usuarios.add(fulano)
		usuarios.add(sultano)
		usuarios.add(mengano)	

		var apuestas = new ArrayList<Apuesta>();
		apuestas.add(apuesta1)
		apuestas.add(apuesta2)
		apuestas.add(apuesta3)
		apuestas.add(apuesta4)
		apuestas.add(apuesta5)
		apuestas.add(apuesta6)
		
		val tendencia1 = new Tendencia(apuesta1.fecha, apuesta1.resultado.ganador)
		val tendencia2 = new Tendencia(apuesta2.fecha, apuesta2.resultado.ganador)
		val tendencia3 = new Tendencia(apuesta3.fecha, apuesta3.resultado.ganador)
		val tendencia4 = new Tendencia(apuesta4.fecha, apuesta4.resultado.ganador)
		val tendencia5 = new Tendencia(apuesta5.fecha, apuesta5.resultado.ganador)
		val tendencia6 = new Tendencia(apuesta6.fecha, apuesta6.resultado.ganador)
		
		var tendencias = new ArrayList<Tendencia>();
		tendencias.add(tendencia1)
		tendencias.add(tendencia2)
		tendencias.add(tendencia3)
		tendencias.add(tendencia4)
		tendencias.add(tendencia5)
		tendencias.add(tendencia6)
		
		var repositorio = new Repositorio(usuarios, apuestas, tendencias);
		
		val appModel = new El8DeLaRiveraAppModel(repositorio)	
		//appModel.repositorio.usuario = fulano
		var El8DeLaRiveraController controller = new El8DeLaRiveraController(appModel)
		
		XTRest.startInstance(controller, 9100)
	
	}	
}
