app.service("apuestasService", function($http){
  
  this.getUsuario = function(nombre="", callbackApuestas) {
      $http.get("/usuario?nombre="+nombre).success(callbackApuestas);
    }

  this.getSaldoU = function(nombre="", callbackApuestas) {
      $http.get("/usuario/saldo?nombre="+nombre).success(callbackApuestas);
    }

  this.getApuestasU = function(nombre="", callbackApuestas) {
      $http.get("/usuario/apuestas?nombre="+nombre).success(callbackApuestas);
    }
  
  this.getResultadoUltimaApuesta = function(nombre="", callbackApuestas) {
      $http.get("/usuario/apuestas/resultado?nombre="+nombre).success(callbackApuestas);
    }

  this.getApuestas = function(callbackApuestas) {
      $http.get("/apuestas").success(callbackApuestas);
    }

  this.getTendencias = function(callbackApuestas) {
      $http.get("/apuestas/tendencias").success(callbackApuestas);
    }

  this.apostar = function(nombre ="", monto="", numero="", callbackApuestas) {
        $http.post("/apostar?nombre="+nombre+"&monto="+monto+"&numero="+numero).success(callbackApuestas);
    }

});

app.service("loginService", function($http){
  this.logearUsuario = function(nombre="", password="", callbackLog) {
      $http.post("/usuarios", {"nombre":nombre, "password":password}).success(callbackLog);
    }
});