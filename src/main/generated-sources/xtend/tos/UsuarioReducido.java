package tos;

import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;

@Accessors
@SuppressWarnings("all")
public class UsuarioReducido {
  private String nombre;
  
  private String password;
  
  public UsuarioReducido(final String nombre, final String password) {
    this.nombre = nombre;
    this.password = password;
  }
  
  public UsuarioReducido() {
    this.nombre = "";
    this.password = "";
  }
  
  @Pure
  public String getNombre() {
    return this.nombre;
  }
  
  public void setNombre(final String nombre) {
    this.nombre = nombre;
  }
  
  @Pure
  public String getPassword() {
    return this.password;
  }
  
  public void setPassword(final String password) {
    this.password = password;
  }
}
