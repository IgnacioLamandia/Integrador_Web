package tos;

import java.util.Date;
import model.Tendencia;
import org.eclipse.xtend.lib.annotations.Accessors;
import org.eclipse.xtext.xbase.lib.Pure;

@Accessors
@SuppressWarnings("all")
public class TendenciaReducida {
  private String numero;
  
  private String fecha;
  
  public TendenciaReducida(final Tendencia tendencia) {
    Date _fecha = tendencia.getFecha();
    int _hours = _fecha.getHours();
    String _string = Integer.valueOf(_hours).toString();
    String _plus = (_string + ":");
    Date _fecha_1 = tendencia.getFecha();
    int _minutes = _fecha_1.getMinutes();
    String _string_1 = Integer.valueOf(_minutes).toString();
    String _plus_1 = (_plus + _string_1);
    this.fecha = _plus_1;
    int _numero = tendencia.getNumero();
    String _string_2 = Integer.valueOf(_numero).toString();
    this.numero = _string_2;
  }
  
  public TendenciaReducida() {
    this.fecha = "";
    this.numero = "";
  }
  
  @Pure
  public String getNumero() {
    return this.numero;
  }
  
  public void setNumero(final String numero) {
    this.numero = numero;
  }
  
  @Pure
  public String getFecha() {
    return this.fecha;
  }
  
  public void setFecha(final String fecha) {
    this.fecha = fecha;
  }
}
